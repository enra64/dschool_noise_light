#include <stdint.h>
#include "EasyBuzzer.h"
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <FastLED.h>
#include <ArduinoLog.h>

#define DHT_PIN            3          // Pin connected to the DHT sensor
#define DHT_TYPE           DHT11      // DHT 11 sensor

#define MIC_PIN            A0         // Pin connected to the microphone

#define NUM_LEDS 10                   // How many leds are in the strip?
#define DATA_PIN 5                    // Data pin that led data will be written out over
CRGB leds[NUM_LEDS];                  // This is an array of leds.  One item for each led in your strip.

DHT dht(DHT_PIN, DHT_TYPE);   // unified sensor instance for dht11

bool currentlyBeeping = false;        // flag to trace whether we are currently within a beep sequence

#define BADNESS_BEEP_THRESHOLD 0.1f   // at this badness (0 to 1), a beep signal will be made

#define MEASUREMENT_TIMEOUT_LENGTH 1500 // timeout for measuring, i.e. this many milliseconds will be warned
unsigned long currentTimeoutEnd = 0;             // millis() at which the current timeout ends

void setup() {
  // Set up serial port and wait until connected
  Serial.begin(115200);
  while(!Serial && !Serial.available()){}
    
  // initialize DHT11
  dht.begin();

  // initialize microphone digital pin
  pinMode(MIC_PIN, INPUT);

  // initialize RGB LED library
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);

  // initialize logging library
  Log.begin(LOG_LEVEL_VERBOSE, &Serial);

  // logging
  Log.notice("Initialization finished\n");
}

void loop() {
  // update easybuzzer library
  EasyBuzzer.update();

  // warn with current badness
  float currentBadness = getCurrentBadness();
  Log.notice(F("Current badness is %F" CR), currentBadness);
  warn(currentBadness);

  // keep leds up to date
  FastLED.show();

  delay(50);
}

void beginTimeout() {
  currentTimeoutEnd = millis() + MEASUREMENT_TIMEOUT_LENGTH;
}

bool inTimeoutPhase() {
  return currentTimeoutEnd > millis();
}

void warn(float badnessFactor) {
  if(inTimeoutPhase()) {
    return;
  }
  
  if (badnessFactor > BADNESS_BEEP_THRESHOLD) {
    Log.notice(F("Sending warning beep" CR));
    beepMultiple(2); 
    beginTimeout();

    badnessFactor = 1.f;
  }
  
  fill_solid(leds, NUM_LEDS, CRGB(int (badnessFactor * 255.f), 255 - ((int) (badnessFactor * 255.f)), 0));
}

float getCurrentBadness() {
  // get source loudness & temperature values
  float loudness = getLoudness();
  float temperature = getTemperature();

  // logging
  //Log.trace(F("Current loudness is %F, current temperature is %F" CR), loudness, temperature);

  // map the values to a "badness" from 0 to 1
  loudness = mapFloat(loudness, 150.f, 1024.f, 0.1f, 1.f);
  temperature = mapFloat(temperature, 23.f, 32.f, 0.1f, 1.f);

  // logging
  //Log.trace(F("Current scaled loudness is %F, current scaled temperature is %F" CR), loudness, temperature);

  // return scaled value of badness
  return loudness * temperature;
}

// get current loudness in analog value
float getLoudness() {
  return analogRead(MIC_PIN);
}

// get current temperature in degrees celsius
float getTemperature() {
  float temperature = dht.readTemperature();
  
  if (isnan(temperature)) {
    Log.fatal(F("Error reading temperature!" CR));
    return -13;
  }
  
  return temperature;
}

// beep "beepCount times"
void beepMultiple(int beepCount) {
  if(currentlyBeeping) {
    return;
  }
  
  currentlyBeeping = true;
  
  EasyBuzzer.beep(
    1000,                   // Frequency in hertz(HZ). 
    50,                     // On Duration in milliseconds(ms).
    100,                    // Off Duration in milliseconds(ms).
    beepCount,              // The number of beeps per cycle.
    500,                    // Pause duration.
    1,                      // The number of cycle.
    beepsFinished           // Callback. A function called  when the sequence ends.
  );
}

// Callback function when the current beep sequence has ended
void beepsFinished() {
  currentlyBeeping = false;
}

// map floats from input to output range
float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
 if ( x < in_min) {
  return out_min;
 }

 if (x > in_max) {
  return out_max;
 }
  
 return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
